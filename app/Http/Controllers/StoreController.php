<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Line;
use App\Order;
class StoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($index=0)
    {
        $products = Product::all();
        return view ('store.index',compact('products'));
    }

        /**
     * Remove a especific line of store.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function addline(Request $request)
    {
        $line = new line();
        $line->product_id = $request->product_id;
        $line->product_id = $request->order_id;
        $line->quantity = $request->quantity;

        $prd = Product::find($product_id);
        $plus = ($prd->category_id == 3) ?  $line->price/1000 : 0;
        
        $line->price = $line->price + $plus;
        
        $line->save();
        return $line;
    }
 
        /**
     * Remove a especific line of store.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delline($id)
    {
        $line = line::find($id);
        $line->delete();
    }


}
