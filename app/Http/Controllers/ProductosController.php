<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($index=0)
    {
        $productos = Product::all();
        return view ('home',compact('productos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $prd = new Product();
        $prd->nombre = $request->nombre;
        $prd->slug = $request->slug;
        $prd->desc = $request->desc;
        $prd->img = $request->img;
        $prd->price = $request->price;
        $prd->category_id = $request->category_id;
        $prd->save();

        return ProductController::index($prd->id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $prd = Product::find($id);
        $prd->nombre = $request->nombre;
        $prd->slug = $request->slug;
        $prd->desc = $request->desc;
        $prd->img = $request->img;
        $prd->price = $request->price;
        $prd->category_id = $request->category_id;
        $prd->save();
        return $prd;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $prd = Product::find($id);
        $prd->delete();
    }
}