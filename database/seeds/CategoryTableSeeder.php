<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Category;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(['nombre' => 'Alimentación',
                       'desc'=> 'productos para comer'],
                    ['nombre' => 'Decoración',
                       'desc'=> 'produtos para decorar tu casa'],
                    ['nombre' => 'Electrónica',
                       'desc'=> 'productos para disfrutar']
                    );

        Category::insert($data);
    }
}

