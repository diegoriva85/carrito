<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Product;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(['nombre' => 'Prod1',
                       'slug'=>'Prod1',
                       'desc'=> 'Lorem Ipsum is simply dummy text ',
                       'img'=>'Prod1',
                       'precio'=>255.00,
                       'category_id'=>1
                    ],
                    ['nombre' => 'Prod2',
                       'slug'=>'Prod2',
                        'desc'=> 'Lorem Ipsum is simply dummy text ',
                      'img'=>'Prod2',
                       'precio'=>25.00,
                       'category_id'=>1
                    ],
                    ['nombre' => 'Prod3',
                       'slug'=>'Prod3',
                        'desc'=> 'Lorem Ipsum is simply dummy text ',
                       'img'=>'Prod3',
                       'precio'=>244.00,
                       'category_id'=>2
                    ],
                    ['nombre' => 'Prod4',
                       'slug'=>'Prod4',
                        'desc'=> 'Lorem Ipsum is simply dummy text ',
                       'img'=>'Prod4',
                       'precio'=>25.00,
                       'category_id'=>2
                    ],
                    ['nombre' => 'Prod5',
                       'slug'=>'Prod5',
                        'desc'=> 'Lorem Ipsum is simply dummy text ',
                       'img'=>'Prod5',
                       'precio'=>255.00,
                       'category_id'=>3
                    ],
                    ['nombre' => 'Prod6',
                       'slug'=>'Prod6',
                        'desc'=> 'Lorem Ipsum is simply dummy text ',
                       'img'=>'Prod6',
                       'precio'=>2.00,
                       'category_id'=>3
                    ],
                    ['nombre' => 'Prod7',
                       'slug'=>'Prod7',
                                               'desc'=> 'Lorem Ipsum is simply dummy text ',
                       'img'=>'Prod7',
                       'precio'=>4.00,
                       'category_id'=>2
                    ],
                    ['nombre' => 'Prod8',
                       'slug'=>'Prod8',
                        'desc'=> 'Lorem Ipsum is simply dummy text ',
                       'img'=>'Prod8',
                       'precio'=>25.00,
                       'category_id'=>3
                    ]);

        Product::insert($data);
    }
}

